"""
The Valve Module provides a class for working with a solenoid valve controlled by a CP2104 USB to UART Bridge
"""
from cp2104Instrument import CP2104Instrument, CP2104InstrumentException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class Valve(CP2104Instrument):
    """The Valve class models a solenoid valve connected to a CP2104 USB to UART Bridge"""

    _namespace = "cp2104.valve"
    _default_settings = {
        # default settings for this class
    }

    INSTRUMENT_TYPE = "valve"
    INSTRUMENT_TYPE_SYNONYMS = {
        "valve"
    }

    VARIABLE_STATE = "state"
    VARIABLES_MEASURED = [VARIABLE_STATE]
    VARIABLES_CONTROLLED = [VARIABLE_STATE]

    def __init__(self, name, parent, settings=None, **kwargs):
        """
        The state of the valve signifies if the valve is open or closed:
         self._state = True - the valve is open
         self._state = False - the valve is closed
        """
        super(Valve, self).__init__(name=name, parent=parent, settings=settings, **kwargs)
        self._state = False

    def setState(self, state):
        """Sets the state of the Valve

        :param state: The state to set the Valve to
        :type state: bool
        """
        result = False
        if self.hasParent() and self.getRelay().hasParent():
            connection = self.getDevice().getConnection()
            if connection is not None and connection.isConnected():
                result = connection.setLatchState(self.index, state)
            else:
                raise CP2104InstrumentException("Instrument cannot change state because there is no connection")
        if result is True:
            self._state = state
        return result

    def getState(self):
        """Returns the state of the Valve

        :rtype: bool
        """
        result = None
        if self.hasParent() and self.getRelay().hasParent():
            connection = self.getDevice().getConnection()
            if connection is not None and connection.isConnected():
                result = connection.getLatchState(self.index)
            else:
                raise CP2104InstrumentException("Instrument cannot get latch state because there is no connection")
        if result is not None:
            self._state = result
        return self._state

    state = property(getState)

    def measure(self, variable, **kwargs):
        result = None
        if variable == self.VARIABLE_STATE:
            result = self.getState()
        return result

    def control(self, variable, value, **kwargs):
        result = False
        if variable == self.VARIABLE_STATE:
            result = self.setState(value)
        return result



