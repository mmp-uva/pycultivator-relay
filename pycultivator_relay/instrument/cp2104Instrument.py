"""The cp2104Instrument model contains classes for modeling Instruments connected to the relays of CP2104 device"""

from pycultivator.instrument import instrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class CP2104Instrument(instrument.Instrument):

    _namespace = "cp2104.instrument"
    _default_settings = {

    }

    def __init__(self, name, parent, settings=None, **kwargs):
        super(CP2104Instrument, self).__init__(name, parent, settings, **kwargs)
        self._address = 0

    def getParent(self):
        """Return the parent of this instrument

        :rtype: pycultivator_relay.device.cp2104.Relay
        """
        return super(CP2104Instrument, self).getParent()

    def getDevice(self):
        """Returns the device object to which this instrument belongs.

        :rtype: pycultivator_relay.device.cp2104.CP2104
        """
        return self.getRelay().getDevice()

    @property
    def device(self):
        return self.getDevice()

    def getRelay(self):
        """Returns the well object to which this instrument belongs.

        :rtype: pycultivator_relay.device.cp2104.Relay
        """
        if not self.hasParent():
            raise CP2104InstrumentException("Instrument does not have a parent")
        return self.getParent()

    @property
    def relay(self):
        return self.getRelay()

    def getRelayIndex(self):
        """Returns the index of the relay in the cp2104

        :rtype: int
        """
        if not self.hasParent():
            raise CP2104InstrumentException("Instrument does not have a parent")
        return self.parseToInteger(self.getParent().getIndex())

    @property
    def index(self):
        return self.getRelayIndex()

    def _getAddress(self):
        return self._address

    def hasAddress(self):
        """Returns whether this CP2104Instrument has a specifically set address.

        If false means; the result of getAddress is guessed using guessAddress

        :rtype: bool
        """
        return self._getAddress() is not None

    def getAddress(self):
        """Returns the address of this CP2104Instrument.

        If the address is not set in the settings, the address will be guessed using guessAddress

        :rtype: int
        """
        result = self._getAddress()
        if result is None:
            result = self.guessAddress()
        return result

    @property
    def address(self):
        return self.getAddress()

    def setAddress(self, address):
        """Sets a custom address for this CP2104Instrument (overrides guessing)

        :param address: The new address of the CP2104Instrument
        :type address: int
        :return: The new address of the CP2104Instrument
        :rtype: int
        """
        self._address = address

    @address.setter
    def address(self, address):
        self.setAddress(address)

    def guessAddress(self):
        """Tries to guess the CP2104Instrument address from the index of the relay where this instrument belongs to"""
        return self.getRelayIndex()

    # Behaviour methods

    def measure(self, variable, **kwargs):
        raise NotImplementedError

    def control(self, variable, value, **kwargs):
        raise NotImplementedError


class CP2104InstrumentException(instrument.InstrumentException):
    """Exception raised by a Cp2104 Instrument"""
    def __init__(self, msg):
        super(CP2104InstrumentException, self).__init__(msg)

