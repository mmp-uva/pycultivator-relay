"""The cp2104 module contains classes for modeling a CP2104 USB to UART Bridge"""

from pycultivator.device import device, channel
from pycultivator_relay.instrument import valve

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class CP2104 (device.ComplexDevice):

    _namespace = "cp2104.device"

    _default_settings = {
        # default settings for this class
    }

    MAX_CHANNELS = 4

    def __init__(self, name=None, parent=None, settings=None,**kwargs):
        super(CP2104, self).__init__(name, parent, settings, **kwargs)

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: pycultivator_relay.connection.cp2104Connection.CP2104Connection
        """
        return super(CP2104, self).getConnection()

    def createConnection(self, fake=False, **kwargs):
        """Creates a new connection and uses it for this device"""
        from pycultivator_relay.connection import cp2104Connection
        if not fake:
            self.setConnection(cp2104Connection.CP2104Connection(**kwargs))
        else:
            raise self.setConnection(cp2104Connection.FakeCP2104Connection(**kwargs))
        return self.getConnection()

    def getChannels(self):
        """ Returns all the relays known by this device

        :rtype: dict[int, pycultivator_relay.device.cp2104.Relay]
        """
        return super(CP2104, self).getChannels()

    def getChannel(self, idx):
        """ Returns the relay at the given index

        :rtype: pycultivator_relay.device.cp2104.Relay
        """
        return super(CP2104, self).getChannel(idx)


class Relay(channel.Channel):
    """A channel in a CP2104"""
    _namespace = "cp2104.relay"

    _default_settings = {
        # default settings for this class
    }

    INSTRUMENTS = {
        valve.Valve: 1,
    }

    def __init__(self, name, parent, settings=None, **kwargs):
        super(Relay, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    def getValve(self):
        """Returns the valve

        :return:
        :rtype: pycultivator_relay.instrument.valve.Valve
        """
        result = None
        instruments = self.getInstrumentsOfClass(valve.Valve)
        if len(instruments) > 0:
            result = instruments.values()[0]
        return result

    def hasValve(self):
        return self.getValve() is not None

    def getCP2104(self):
        """ Returns the CP2104 object to which this relay belongs.

        :return: The cp2104 object
        :rtype: pycultivator_relay.device.cp2104.CP2104
        """
        return self.getDevice()

    def getDevice(self):
        """ Returns the CP2104 object to which this relay belongs.

        :return: The cp2104 object
        :rtype: pycultivator_relay.device.cp2104.CP2104
        """
        return super(Relay, self).getDevice()

    def setValveState(self, state):
        """Sets the state of the valve
        :type state: bool
        """
        result = False
        valve = self.getValve()
        if valve is not None:
            result = valve.setState(state=state)
        return result


class CP2104Exception(device.DeviceException):
    """An exception raised by the LightCalibrationPlate class"""

    pass


