from time import sleep
from . import LiveCP2104TestCase

__author__ = "Max Guillaume <maxguillaume20@gmail.com>"


class ValveTestCase (LiveCP2104TestCase):

    @classmethod
    def setUpClass(cls):
        """Prepares the class for running"""
        super(ValveTestCase, cls).setUpClass()
        cls.report("###################################")
        cls.report("This test uses the CP2104 USB to UART Bridge")
        cls.report("Connection will {} simulated".format("BE" if cls.usesFake() else "NOT BE"))
        cls.report("###################################")

    def test_hasCP2104(self):
        self.assertIsNotNone(self.getCP2104())

    def test_instruments(self):
        self.assertEqual(self.getCP2104().countChannels(), 4)
        instrumentType = list(self.getCP2104().getInstrumentTypes())[0]
        self.assertEqual(instrumentType, 'valve')
        self.reportState()
        self.report("## Turning on all relays ##")
        self.getCP2104().control(instrument=instrumentType, variable='state', value=True)
        self.reportState()
        sleep(1)
        self.report("## Turning off all relays ##")
        self.getCP2104().control(instrument=instrumentType, variable='state', value=False)
        self.reportState()

    def reportState (self):
        results = self.getCP2104().measure(instrument='valve', variable='state')
        for c in self.getCP2104().getChannels().values():
            self.report("The state of instrument {} is {}".format(c.index, results[c.index]))



