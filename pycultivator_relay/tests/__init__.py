from pycultivator_relay.device.cp2104 import CP2104
from pycultivator_relay.config import xmlConfig
from pycultivator.core.tests import UvaSubjectTestCase
import os, sys, pkg_resources

__author__ = "Max Guillaume <maxguillaume20@gmail.com>"


class LiveCP2104TestCase(UvaSubjectTestCase):

    PLATE_CONFIG_PATH = pkg_resources.resource_filename(
        __name__, os.path.join("..", "config", "schema", "configuration.xml")
    )

    _use_fake = os.getenv("USE_REAL", "0") == "0"
    _cp2104 = None
    _subject_cls = CP2104

    @classmethod
    def loadCP2104(cls, path=None, settings=None, **kwargs):
        """ Loads the configuration object and device from the given XML Configuration file path into a new script

        :param path: Path to the XML Configuration file
        :type path: str
        :param settings: Settings to be passed to the configuration
        :type settings: None or dict
        :return:
        :rtype: pycultivator_relay.device.cp2104.CP2104
        """
        result = False
        if path is None:
            path = cls.PLATE_CONFIG_PATH
        c = xmlConfig.XMLConfig.load(path, settings=settings, **kwargs)
        if c is not None:
            # create device configuration object
            xdc = c.getHelperFor("device")
            # create device object
            d = xdc.load()
            """:type: pycultivator_relay.device.cp2104.CP2104"""
            if d is not None:
                cls.setCP2104(d)
                result = cls.hasCP2104()
        return result

    @classmethod
    def getCP2104(cls):
        """Return the cp2104 object or nothing if no cp2104 is set

        :rtype: pycultivator_relay.device.cp2104.CP2104 or None
        """
        return cls._cp2104

    @classmethod
    def hasCP2104(cls):
        """Returns whether a cp2104 is set for this test

        :rtype: bool
        """
        return cls.getCP2104() is not None

    @classmethod
    def setCP2104(cls, cp2104):
        """Sets the cp2104 object that is used in this test

        :type plate: pycultivator_relay.device.cp2104.CP2104
        """
        cls._cp2104 = cp2104
        return cls.getCP2104()

    @classmethod
    def usesFake(cls):
        """Whether this test uses a fake cp2104 connection, thus simulating a cp2104"""
        return cls._use_fake

    @classmethod
    def getRelays(cls):
        """Return a list of relays in the cp2104, if a cp2104 is set

        :rtype: None or list[pycultivator_relay.device.cp2104.Relay]
        """
        result = None
        if cls.hasCP2104():
            result = cls.getCP2104().getChannels().values()
        return result

    @classmethod
    def setUpClass(cls):
        """Prepares the class for running"""
        super(LiveCP2104TestCase, cls).setUpClass()
        # see if a fake connection should be made
        cls.getLog().info(
            "Will {} simulate the connection to the cp2104 device".format("NOT" if cls.usesFake() else "")
        )
        # load configuration classes
        cls.loadCP2104(settings={"fake.connection": cls.usesFake()})
        if not cls.hasCP2104():
            raise AssertionError("Unable to load configuration file")
        result = cls.getCP2104().connect()
        if not result:
            raise AssertionError("Unable to connect to cp2104")

    @classmethod
    def tearDownClass(cls):
        if cls.getCP2104().isConnected():
            result = cls.getCP2104().disconnect()
            if not result:
                raise AssertionError("Unable to disconnect cp2104")

    @classmethod
    def report(cls, msg, end="\n"):
        sys.stderr.write("{}{}".format(msg, end))
        sys.stderr.flush()
