"""Console Script for listing USB Devices connected to this computer"""


def list_relay_devices(idVendor=None, idProduct=None):
    """List all devices"""
    from pycultivator_relay.connection import cp2104Connection
    return cp2104Connection.CP2104Connection.find(vendor=idVendor, product=idProduct)


def print_relay_devices(idVendor=None, idProduct=None):
    """Print an overview of all devices"""
    devices = list_relay_devices(idVendor, idProduct)
    vendor = "any" if idVendor is None else idVendor
    product = "any" if idProduct is None else idProduct
    print(
        "Found {} USB relay boards [matching: idVendor = {} and idProduct = {}]".format(
            len(devices), vendor, product
        )
    )
    _print_table_edge("=")
    _print_table_line("Vendor ID", "Product ID", "Bus", "Address")
    _print_table_edge("=")
    for idx, device in enumerate(devices):
        _print_table_line(device.idVendor, device.idProduct, device.bus, device.address)
        if idx < (len(devices) - 1):
            _print_table_edge()
    _print_table_edge()


def _print_table_edge(sep="-", columns=4):
    sep = "".join(sep for __ in range(20))
    msg = "+".join([sep for __ in range(columns)])
    print ("+{}+".format(msg))


def _print_table_line(vendor, product, bus, address):
    values = [vendor, product, bus, address]
    msg = "|".join(["{:^20s}".format(str(v)) for v in values])
    print ("|{}|".format(msg))


def run():
    import argparse

    p = argparse.ArgumentParser("List USB Devices")
    p.add_argument(
        "--vendor",
        help="Only show USB Devices with this vendor ID (hexadecimal)"
    )
    p.add_argument(
        "--product",
        help="Only show USB Devices with this product ID (hexadecimal)"
    )
    args = vars(p.parse_args())
    vendor = None
    try:
        vendor = args.get("vendor")
        if vendor is not None and vendor.lower() == "any":
            vendor = None
        if vendor is not None:
            vendor = int(vendor, 16)
    except ValueError:
        print("Invalid value for vendor, need a hexadecimal number!")
        exit(-1)
    product = None
    try:
        product = args.get("product")
        if product is not None and product.lower() == "any":
            product = None
        if product is not None:
            product = int(product, 16)
    except ValueError:
        print("Invalid value for product, need a hexadecimal number!")
        exit(-1)
    print_relay_devices(vendor, product)

if __name__ == "__main__":
    run()