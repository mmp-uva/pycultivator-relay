"""Console Script for listing USB Devices connected to this computer"""

from pycultivator_relay.connection import cp2104Connection


def run():
    import argparse

    p = argparse.ArgumentParser("Configure the latch state of a CP2104 device")
    p.add_argument(
        "latch", type=int, choices=range(8),
        help="Latch index to configure"
    )
    p.add_argument(
        "state", type=int, choices=(0, 1),
        help="State to set latch to"
    )
    p.add_argument(
        "--bus", dest="usb_bus", type=int,
        help="Configure the device connected at this bus number"
    )
    p.add_argument(
        "--address", dest="usb_address", type=int,
        help="Configure the device connected at this address"
    )
    p.add_argument(
        "-f", "--fake", dest="use_fake", action="store_true",
        help="Simulate the connection to the device"
    )
    args = vars(p.parse_args())
    usb_bus = args.get("usb_bus")
    if usb_bus is not None:
        usb_bus = str(usb_bus)
    usb_address = args.get("usb_address")
    if usb_address is not None:
        usb_address = str(usb_address)
    use_fake = args.get("use_fake")
    if use_fake is None:
        use_fake = False
    if use_fake:
        dev = cp2104Connection.FakeCP2104Connection()
    else:
        dev = cp2104Connection.CP2104Connection()
    if None in (usb_address, usb_bus):
        print("Will connect to first device matching")
    result = dev.connect(bus=usb_bus, address=usb_address)
    print("Connect to {}device at [Bus: {}, Address: {}]".format(
            "" if use_fake else "FAKE ", usb_bus, usb_address
        )
    )
    if not result:
        exit(-1)
    latch = args.get("latch")
    state = args.get("state")
    result = dev.setLatchState(latch, state)
    print("Set Latch {} to {}: {}".format(latch, state, result))

if __name__ == "__main__":
    run()