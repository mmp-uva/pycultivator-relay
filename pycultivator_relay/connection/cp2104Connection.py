"""Connection driver to the CP210x USB to UART bridge

Documentation on USB Protocol:

- https://github.com/walac/pyusb/blob/master/docs/tutorial.rst
- http://www.silabs.com/documents/public/application-notes/AN571.pdf
"""
from usbConnection import USBConnection, FakeUSBConnection, USBConnectionException
import usb.util

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class CP2104Connection(USBConnection):

    _namespace = "cp2104.connection.cp2104"
    _default_settings = {
        # default settings for this class
    }

    USB_VENDOR_ID = 0x10c4
    USB_PRODUCT_ID = 0xea60

    # Request Types
    USB_REQUEST_TYPE_QUERY = usb.util.CTRL_IN | usb.util.CTRL_TYPE_VENDOR
    USB_REQUEST_TYPE_WRITE = usb.util.CTRL_OUT | usb.util.CTRL_TYPE_VENDOR
    # Request codes
    USB_REQUEST_VENDOR = 0xFF
    # Request cmds
    CP201X_SET_LATCH = 0x37E1
    CP201X_GET_LATCH = 0x00C2

    def __init__(self, settings=None, **kwargs):
        super(CP2104Connection, self).__init__(settings=settings, **kwargs)

    def connect(self, dev=None, bus=None, address=None):
        if self.isConnected():
            raise USBConnectionException("Already connected, disconnect first")
        if dev is None:
            devs = self.find(vendor=self.USB_VENDOR_ID, product=self.USB_PRODUCT_ID, bus=bus, address=address)
            if len(devs) > 0:
                dev = devs[0]
            else:
                raise USBConnectionException("Unable to make connection with a CP2104 device")
        super(CP2104Connection, self).connect(dev)
        return self.isConnected()

    def query(self, request, value, index, length):
        """Queries the relay via the Control Transfer"""
        return self.transfer(self.USB_REQUEST_TYPE_QUERY, request, value, index, length)

    def write(self, request, value, index, data):
        """Writes to the relay via Control Transfer"""
        return self.transfer(self.USB_REQUEST_TYPE_WRITE, request, value, index, data)

    def setLatchStates(self, mask=0xf, states=0x0):
        """Set the state of all relays

        :param mask: Which relays are affected. Bit 0 (right-most) = relay 0
        :param states: New state of relays. Bit 0 (right-most) = relay 0
        """
        self.constrain(mask, 0x0, 0xf, name="mask")
        self.constrain(states, 0x0, 0xf, name="states")
        msg = (states << 8) | mask
        self.write(self.USB_REQUEST_VENDOR, self.CP201X_SET_LATCH, msg, 0)
        return True

    def setLatchState(self, index, state):
        self.constrain(index, 0, 8, name="index")
        mask = 1 << index
        states = (0 if state else 1) << index
        self.getLog().info("Mask: {:08b}".format(mask))
        self.getLog().info("States: {:08b}".format(states))
        return self.setLatchStates(mask=mask, states=states)

    def getLatchStates(self, mask=0xf):
        """Retrieve the states of all relays

        :rtype: list[bool]
        """
        result = []
        states = self.query(self.USB_REQUEST_VENDOR, self.CP201X_GET_LATCH, 0, mask)
        if len(states) > 0:
            states = states[0]
        for i in range(8):
            result.append(states & (1 << i) == 0)
        return result

    def getLatchState(self, index):
        """Retrieve the state of a single relay"""
        result = False
        self.constrain(index, 0, 7, name="index", include=True)
        mask = 1 << index
        states = self.getLatchStates(mask)
        # now retrieve value
        return states[index]


class FakeCP2104Connection(CP2104Connection, FakeUSBConnection):

    def __init__(self, settings=None, **kwargs):
        super(FakeCP2104Connection, self).__init__(settings=settings, **kwargs)
        self._states = [False for __ in range(8)]

    def connect(self, dev=None, bus=None, address=None):
        return FakeUSBConnection.connect(self, dev)

    def disconnect(self):
        return FakeUSBConnection.disconnect(self)

    def isConnected(self):
        return FakeUSBConnection.isConnected(self)

    def reset(self):
        return FakeUSBConnection.reset(self)

    def transfer(self, requestType, request, value, index, data):
        return FakeUSBConnection.transfer(self, requestType, request, value, index, data)

    def setLatchState(self, index, state):
        self._states[index] = state
        return True

    def getLatchState(self, index):
        self.constrain(index, 0, 7, name="index", include=True)
        return self._states[index]
