# coding=utf-8
"""Test cases for the packet module"""

from pycultivator.connection.tests import test_SerialConnection
from pycultivator.foundation.tests import _test
from pycultivator_plate.connection import PlateConnection
import serial

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestPlateConnection(test_SerialConnection.TestSerialConnection):
    """Test case for the Plate Connection class"""

    _subject_cls = PlateConnection.PlateConnection
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """ Returns the class of the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.PlateConnection
        """
        return super(TestPlateConnection, cls).getSubjectClass()

    def getSubject(self):
        """Returns the subject of the test case

        :return:
        :rtype: pycultivator_plate.connection.PlateConnection.PlateConnection
        """
        return super(TestPlateConnection, self).getSubject()
