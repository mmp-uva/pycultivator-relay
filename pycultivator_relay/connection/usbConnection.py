""" Generic USB Connection driver

"""

from pycultivator.connection import Connection
import usb.core
import usb.util

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class USBConnection(Connection.Connection):
    """Models a connection over USB using pysub"""

    _namespace = "cp2104.connection.usb"
    _default_settings = {
        # default settings for this class
    }

    # Vendor Id's
    USB_VENDOR_ID = 0x00
    USB_PRODUCT_ID = 0x01

    def __init__(self, settings=None, **kwargs):
        super(USBConnection, self).__init__(settings=settings, **kwargs)
        self._has_kernel_driver = False
        self._interface = None

    @classmethod
    def find(cls, find_all=True, vendor=None, product=None, bus=None, address=None):
        """Finds all connected usb devices, with matching vendor id, product id, bus, and address,
        if those arguements are assigned

        :param find_all: Whether to return all matches or only first
        :rtype: list[usb.core.Device]
        """
        # put arguements in a dictionary
        kwargs = {"idVendor": vendor, "idProduct": product, "bus": bus, "address": address}
        # remove unassigned arguments
        kwargs = {key:value for key, value in kwargs.items() if value is not None}
        return list(usb.core.find(find_all=find_all, **kwargs))

    def getDevice(self):
        """Return the handle to the USB Device

        :return:
        :rtype: usb.core.Device or None
        """
        return self.device

    def setDevice(self, dev):
        self.device = dev

    @property
    def device(self):
        return self._device

    @device.setter
    def device(self, dev):
        # if not isinstance(dev, usb.core.Device):  # change to usb.core.Device from usb.Device
        #     raise ValueError("Invalid device handle")
        self._device = dev

    def connect(self, dev=None):
        if dev is None:
            dev = self.find(find_all=False)
            if len(dev) > 0:
                dev = dev[0]
            else:
                dev = None
        self.setDevice(dev)
        # FROM CP210x Programmer: https://sourceforge.net/p/cp210x-program
        self._has_kernel_driver = self.device.is_kernel_driver_active(0)
        if self._has_kernel_driver:
            cfg = self.device.get_active_configuration()
            self._interface = cfg[(0,0)].bInterfaceNumber
            self.device.detach_kernel_driver(self._interface)
        # load default configuration
        self.device.set_configuration()
        self._isOpen = True
        return self.isConnected()

    def disconnect(self):
        # FROM CP210x Programmer: https://sourceforge.net/p/cp210x-program
        if self.hasDevice() and self._has_kernel_driver:
            self.device.attach_kernel_driver(self._interface)
            self._device = None
            self._isOpen = False
        return not self.isConnected()

    def reset(self):
        if self.hasDevice():
            self.device.reset()

    def transfer(self, requestType, request, value, index, data):
        """Transfers information between the host and the USB"""
        result = None
        if not self.isConnected():
            raise USBConnectionException("Not connected to a device")
        try:
            result = self.device.ctrl_transfer(
                requestType, request, value, index, data
            )
        except usb.core.USBError as e:
            self.getLog().error("Unable to transfer request: {}".format(e))
        return result


class USBConnectionException(Connection.ConnectionException):

    def __init__(self, msg):
        super(USBConnectionException, self).__init__(msg)


class FakeUSBConnection(USBConnection, Connection.FakeConnection):

    def connect(self, dev=None):
        self._isOpen = True
        return self._isOpen is True

    def disconnect(self):
        if self.isConnected():
            self._isOpen = False
        return self._isOpen is False

    def isConnected(self):
        return self._isOpen is True

    def reset(self):
        return True

    def transfer(self, requestType, request, value, index, data):
        if not self.isConnected():
            raise USBConnectionException("Not connected to a device")
        return 0
