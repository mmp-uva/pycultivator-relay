"""This module provides an API for serializing the configuration setting of the 24-well plate incubator"""

from pycultivator.config import baseConfig
from pycultivator_relay.device import cp2104
from pycultivator_relay.connection import cp2104Connection
from pycultivator_relay.instrument import valve

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class DeviceConfig(baseConfig.ComplexDeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    _configures = cp2104.CP2104

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, Config):
            raise ConfigException("Expected a cp2104 Configuration Source")
        super(DeviceConfig, self).__init__(source=source, settings=settings, **kwargs)

    @classmethod
    def assertDefinition(cls, definition, expected=None):
        raise NotImplementedError

    def find(self, name=None, root=None):
        raise NotImplementedError

    def readDefaultChannel(self, definition, d, template=None):
        return super(DeviceConfig, self).readDefaultChannel(definition, d, template=template)

    def save(self, part, root=None):
        raise NotImplementedError


class ConnectionConfig(baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = cp2104Connection.CP2104Connection

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object of a device

        :type source: pycultivator_relay.config.baseConfig.Config
        """
        if not isinstance(source, Config):
            raise ConfigException("Expected a cp2104 Configuration Source")
        super(ConnectionConfig, self).__init__(source=source, settings=settings, **kwargs)

    @classmethod
    def assertDefinition(cls, definition, expected=None):
        raise NotImplementedError

    def find(self, root=None):
        raise NotImplementedError

    @classmethod
    def loadDefault(cls):
        return cp2104Connection.CP2104Connection()

    @classmethod
    def loadFakeDefault(cls):
        return cp2104Connection.FakeCP2104Connection()

    def save(self, o, root=None):
        raise NotImplementedError


class RelayConfig(baseConfig.ChannelConfig):
    """Config class to handle the channel configuration from the configuration source"""

    _configures = cp2104.Relay
    _configures_type = {"relay"}

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, Config):
            raise ConfigException("Expected a cp2104 Configuration Source")
        super(RelayConfig, self).__init__(source=source, settings=settings, **kwargs)

    @classmethod
    def assertDefinition(cls, definition, expected=None):
        raise NotImplementedError

    def find(self, name=None, root=None):
        raise NotImplementedError

    # Behaviour methods

    def readInstruments(self, element, relay, template=None):
        # load the led instruments
        valveHelper = self.getSource().getHelperFor("valve")
        """:type: pycultivator_plate.config.XMLConfig.XMLLEDConfig"""
        instruments = []
        if valveHelper is not None:
            instruments.extend(valveHelper.loadAll(root=element, parent=relay, template=template))
        # add all found instruments
        for instrument in instruments:
            relay.setInstrument(instrument.getName(), instrument)
        return relay

    def save(self, part, root=None):
        raise NotImplementedError


class ValveConfig(baseConfig.InstrumentConfig):
    """Config class to handle the Valve configuration from the configuration source"""

    _configures = valve.Valve
    _configures_types = {"valve"}

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object of a device

        :type source: pycultivator_relay.config.baseConfig.Config
        """
        if not isinstance(source, Config):
            raise ConfigException("Expected a cp2104 Configuration Source")
        super(ValveConfig, self).__init__(source=source, settings=settings, **kwargs)

    @classmethod
    def assertDefinition(cls, definition, expected=None):
        raise NotImplementedError

    def find(self, name=None, root=None):
        raise NotImplementedError

    # Behaviour methods

    def save(self, part, root=None):
        raise NotImplementedError


class Config(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object."""

    _namespace = "config"
    # set available helpers
    _known_helpers = [
        DeviceConfig,
        ConnectionConfig,
        RelayConfig,
        ValveConfig,
        baseConfig.CalibrationConfig,
        baseConfig.PolynomialConfig,
        baseConfig.SettingsConfig
    ]

    default_settings = {
        # default settings for this class
    }

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)

    @classmethod
    def load(cls, location, settings=None, **kwargs):
        """ABSTRACT Loads the source into memory and creates the config object

        :param location: Location from which the configuration will be loaded
        :type location: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate.config.baseConfig.Config
        """
        raise NotImplementedError

    @classmethod
    def check(cls, location):
        """ABSTRACT Checks whether the source can be loaded into the configuration object

        :param location: Location that has to be checked
        :type location: str
        :rtype: bool
        """
        raise NotImplementedError

class ConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
