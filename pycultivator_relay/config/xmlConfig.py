"""This module provides an API for serializing the configuration setting of the CP2104 USB to UART Bridge"""

from pycultivator_relay.config import baseConfig
from pycultivator.config import xmlConfig as baseXMLConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>, Max Guillaume <maxguillaume20@gmail.com>'


class XMLDeviceConfig(baseXMLConfig.XMLComplexDeviceConfig, baseConfig.DeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    XML_ELEMENT_TAG = "cp2104"

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise XMLConfigException("Expected a CP2104 Configuration Source")
        super(XMLDeviceConfig, self).__init__(source=source, settings=settings, **kwargs)

    @classmethod
    def loadDefault(cls, parent=None):
        return baseConfig.DeviceConfig.loadDefault(parent)


class XMLConnectionConfig(baseXMLConfig.XMLConnectionConfig, baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a cp2104 connection object from a configuration source"""

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise XMLConfigException("Expected a CP2104 Configuration Source")
        super(XMLConnectionConfig, self).__init__(source=source, settings=settings, **kwargs)

    def read(self, element, template):
        """Creates a new Connection object from a XML Element

        :type element: lxml.etree._Element._Element
        :type template: None or pycultivator_relay.connection.cp2104Connection.CP2104Connection
        :rtype: pycultivator_relay.connection.cp2104Connection.CP2104Connection
        """
        connection = super(XMLConnectionConfig, self).read(element, template)
        return connection

    @classmethod
    def loadDefault(cls):
        return baseConfig.ConnectionConfig.loadDefault()

    @classmethod
    def loadFakeDefault(cls):
        return baseConfig.ConnectionConfig.loadFakeDefault()

    # def loadBoard(self, element, parent=None, template=None):
    #     index = self.getXML().getId(element=element, vtype=int, default=None)
    #     port = self.getXML().getAttribute(element=element, name="port", vtype=str, default=None)
    #     return index, port


class XMLRelayConfig(baseXMLConfig.XMLChannelConfig, baseConfig.RelayConfig):
    """Config class to handle the channel configuration from the configuration source"""

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise XMLConfigException("Expected a CP2104 Configuration Source")
        super(XMLRelayConfig, self).__init__(source=source, settings=settings, **kwargs)

    # Behaviour methods

    @classmethod
    def loadDefault(cls, parent=None):
        return baseConfig.RelayConfig.loadDefault(parent=parent)


class XMLInstrumentConfig(baseXMLConfig.XMLInstrumentConfig, baseConfig.baseConfig.InstrumentConfig):

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise XMLConfigException("Expected a CP2104 Configuration Source")
        super(XMLInstrumentConfig, self).__init__(source=source, settings=settings, **kwargs)

    def read(self, element, template):
        """Parses the Instrument Configuration into a Object

        :type element: lxml.etree._Element._Element
        :type template: None or pycultivator_relay.instrument.cp2104Instrument.CP2104Instrument
        :rtype: pycultivator_relay.instrument.cp2104Instrument.CP2104Instrument
        """
        instrument = super(XMLInstrumentConfig, self).read(element, template=template)
        # """:type: pycultivator_plate.instrument.PlateInstrument.PlateInstrument"""
        # address = self.getXML().getAttribute(element, "address", vtype=int, default=None)
        # """:type: None | int"""
        # if address is not None:
        #     instrument.setAddress(address)
        return instrument

    def write(self, instrument, element):
        """ Writes od to the configuration

        :param instrument: pycultivator_plate.instrument.PlateInstrument.PlateInstrument
        :type element: lxml.etree._Element._Element
        :return: bool
        """
        result = super(XMLInstrumentConfig, self).write(instrument, element)
        # TODO: write instruments information
        return result


class XMLValveConfig(XMLInstrumentConfig, baseConfig.ValveConfig):
    """Config class to handle the OD Sensor configuration from the configuration source"""

    XML_ELEMENT_TAG = "valve"
    XML_ELEMENT_SCOPE = ".//"

    # Behaviour methods

    def read(self, element, template):
        valve = super(XMLInstrumentConfig, self).read(element, template=template)
        return valve

    @classmethod
    def loadDefault(cls, parent=None):
        return baseConfig.ValveConfig.loadDefault(parent=parent)


class XMLConfig(baseXMLConfig.XMLConfig, baseConfig.Config):
    """Config class for handling the CP2104 XML File"""

    # set available helpers
    _known_helpers = [
        # baseXMLConfig.XMLObjectConfig,
        # baseXMLConfig.XMLPartConfig,
        XMLDeviceConfig,
        XMLConnectionConfig,
        XMLRelayConfig,
        XMLValveConfig,
        baseXMLConfig.XMLCalibrationConfig,
        baseXMLConfig.XMLPolynomialConfig,
        baseXMLConfig.XMLSettingsConfig,
    ]

    COMPATIBLE_VERSION = "1.0"
    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_relay.xsd")

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the source into memory and creates the config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate.config.baseConfig.Config
        """
        return super(XMLConfig, cls).load(path, settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
