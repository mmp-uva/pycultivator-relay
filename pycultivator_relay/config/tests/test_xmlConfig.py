import os
from pycultivator.config.tests import test_xmlConfig, test_baseConfig
from pycultivator_relay.config import xmlConfig

class TestXMLConfig(test_xmlConfig.TestXMLConfig):
    """Class for testing the lcp configuration object"""

    _abstract = False
    _subject_cls = xmlConfig.XMLConfig

    _schemas_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
    _config_path = os.path.join(_schemas_path, "configuration.xml")
    _schema_path = os.path.join(_schemas_path, "pycultivator_relay.xsd")

# class TestXMLDeviceConfig(TestXMLObjectConfig, test_baseConfig.TestDeviceConfig):
#     """Class for testing the XMLDeviceConfig object"""
#
#     _abstract = True
#     _subject_cls = xmlConfig.XMLDeviceConfig