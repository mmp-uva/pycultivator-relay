from pycultivator.config.tests import test_baseConfig
from pycultivator_relay.config import baseConfig

class TestDeviceConfig(test_baseConfig.TestPartConfig):
    """Class for testing the pycultivator_relay DeviceConfig object"""

    _abstract = True
    _subject_cls = baseConfig.DeviceConfig


class TestConnectionConfig(test_baseConfig.TestPartConfig):
    """Class for testing the pycultivator ChannelConfig object"""

    _abstract = True
    _subject_cls = baseConfig.ConnectionConfig

class TestRelayConfig(test_baseConfig.TestPartConfig):
    """Class for testing the pycultivator ChannelConfig object"""

    _abstract = True
    _subject_cls = baseConfig.RelayConfig

class TestValveConfig(test_baseConfig.TestPartConfig):
    """Class for testing the pycultivator ChannelConfig object"""

    _abstract = True
    _subject_cls = baseConfig.ValveConfig