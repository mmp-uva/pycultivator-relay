=========
CHANGELOG
=========

Version 1.0.3.dev1
------------------

* [Bug] Fix return value in FakeUSBConnection.connect()
* [Enhancement] Catch USBError in transfer method

Version 1.0.2.dev1
------------------

* [Feature] Implement Fake CP2104 Connection
* [Bug] Fix bug in argparse of device discovery
* [Enhancement] Log mask and state values in setLatchState
* [Enhancement] Drop requirement for SI Labs driver

Version 1.0.1.dev1
------------------

* [Feature] Add console utility for device discovery

Version 1.0.0.dev1
------------------

* First version
