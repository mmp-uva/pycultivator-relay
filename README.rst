################################
PyCultivator Relay Board Package
################################

Provides bindings to a relay board using the SILabs CP210x USB-UART Bridge.

----

============
Requirements
============

* pyUSB
* pycultivator

The package will communicate to the device directly using the USB specification, it is therefore not necessary to install drivers.

Installation
------------

Simply install using pip:

.. code-block:: bash

    pip install git+https://gitlab.com/pycultivator-relay.git

For some systems, administration (super-user) rights are required for communication with USB devices.
On linux systems with `udev` installed, a simple rule can be added, to allow users to connect to the relay (or any CP210x controlled device).
The following has to be stored in `/etc/udev/rules.d/99-cp201x.rules` (or equivalent location):

.. code-block:: bash

    SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", GROUP="dialout", MODE="0666"

Usage
-----

Two small console utilities are provided:

* list_usb_devices
* configure_cp2104

`list_usb_devices` lists the Bus and Address information of all connected USB devices, matching the provided vendor and product specifications.

Get an overview of all USB connected devices:

.. code-block:: bash

    list_usb_devices

Get an overview of all connected CP2104 devices (Vendor ID: 0x10c4, Product ID: 0xea60)

.. code-block:: bash

    list_usb_devices --vendor 0x10c4 --product 0xea60

`configure_cp2104` helps